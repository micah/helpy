require 'test_helper'
require 'minitest/mock'

class AttachmentSettingTest < ActiveSupport::TestCase

  setup do
    set_default_settings
  end

  # TODO handle attachments
  test 'an email with one attachment should be saved without that attachment' do

    EmailProcessor.stub :attachments_enabled?, false do
      refute EmailProcessor.attachments_enabled?
      assert_difference('Topic.count', 1) do
        assert_difference('Post.count', 1) do
          EmailProcessor.new(build(:email_from_unknown_with_attachments, :with_attachment)).process
        end
      end

      assert_equal [], Post.last.attachments
      assert_equal 0, Topic.last.posts.first.attachments.count
    end
  end

  test 'an email with multiple attachments should be saved without those attachments' do
    EmailProcessor.stub :attachments_enabled?, false do
      assert_difference('Topic.count', 1) do
        assert_difference('Post.count', 1) do
          EmailProcessor.new(build(:email_from_unknown_with_attachments, :with_multiple_attachments)).process
        end
      end
      assert_equal [], Post.last.attachments
      assert_equal 0, Topic.last.posts.first.attachments.count
    end
  end

end
