// hack to prevent exception from being raised when page is first loaded.
var Helpy = Helpy || {};
Helpy.showGroup = function() {}

// dummy stub to replace google analytics tracking.
Helpy.track = function() {}
